## VERSIONS

### 0.1.0
- Add : Garden scene to plant flowers
- Add : Generic type plant for garden
- Add : Menu to select type of plant to plant
- Add : Watering plant with click
- Add : Plant grows when it has water