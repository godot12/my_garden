extends Plant

# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func grow():
	var flower_scene := preload("res://src/Flower.tscn")
	var flower := flower_scene.instance()
	self.get_parent().add_child(flower)
	flower.position = self.position
	


func _process(delta):
	pass
	
func _input(event):
	if event.is_action_pressed("click"):
		_water = 3
		
