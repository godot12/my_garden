extends Plant


func _ready():
	pass
	
func grow():
	var bud_scene := preload("res://src/Bud.tscn")
	var bud := bud_scene.instance()
	self.get_parent().add_child(bud)
	bud.position = self.position
	
func _process(delta):
	pass
	
func _input(event):
	if event.is_action_pressed("click") and _is_mouse_on:
		_water = 3
		
func _on_Grass_mouse_entered():
	_is_mouse_on = true

func _on_Grass_mouse_exited():
	_is_mouse_on = false

