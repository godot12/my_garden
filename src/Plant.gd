extends KinematicBody2D
class_name Plant

var _water := 0.0
var _growth := 0.0
var _is_mouse_on := false

# Called when the node enters the scene tree for the first time.
func _ready():
	input_pickable = true
	
func _process(delta):
	if _water > 0:
		_water -= delta
		_growth += delta
		print(_growth)
		print(_water)
		
	if _growth >= 0.5:
		grow()
		queue_free()

func grow():
	pass
	

